def peliculas_view(request):


    # obtiene peliculas
    lista_peliculas=Pelicula.objects.order_by('titulo')

    contexto={
        'peliculas':lista_peliculas
    }
    return render(request,'app/peliculas.html',contexto)


def form_crear_pelicula_view(request):

    # obtiene las categorias
    lista_categorias= Categoria.objects.all().order_by('-nombre')
    # crear contexto
    contexto={
        'categorias': lista_categorias
    }
    return render(request,'app/crearPelicula.html',contexto)

def crear_pelicula_post(request):
    # obtener datos formu
    titulo=request.POST['titulo']
    year=int(request.POST['year'])
    sinopsis=request.POST['sinopsis']
    id_categoria=int(request.POST['categoria'])
    # obtiene la categoria
    categoria=Categoria.objects.get(id= id_categoria) 
    # crear pelicula
    p=Pelicula()
    p.titulo=titulo
    p.year= year
    p.sinopsis=sinopsis
    p.categoria=categoria
# guardar pelic
    p.save()
    return redirect ('app:peliculas')