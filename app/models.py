
from django.db import models
from django.db.models.deletion import PROTECT
from django.contrib.auth.models import User

# Create your models here.


class Individuo (models.Model):
    
    apellidos= models.CharField(max_length=45,null=False)
    nombres= models.CharField(max_length=45,null=False)
    fechaDeNacimiento= models.DateField(null=True)
    aprobado=models.BooleanField(null=True)
    visitas=models.IntegerField(null=True)
    creador= models.ForeignKey(
        User,
        related_name='individuos',
        null=True,
        on_delete=models.PROTECT

    )

    class Meta: 
        app_label: 'app'

class Partido (models.Model):
    nombre= models.CharField(max_length=45,null=False)
    visitas=models.IntegerField(null=True)
    creador= models.ForeignKey(
        User,
        related_name='partidos',
        null=True,
        on_delete=models.PROTECT

    )
   

    class Meta: 
        app_label: 'app'


class Proceso(models.Model):
    titulo=models.CharField(max_length=45,null=False)
    fecha_inicio=models.DateField(null=True) 
    fecha_fin=models.DateField(null=True)
    abierto=models.BooleanField(null=True)
    entidad=models.CharField(max_length=45,null=False)
    monto=models.IntegerField(null=True)
    comentarios=models.TextField(null=True)
    aprobado=models.BooleanField(null=True)
    visitas=models.IntegerField(null=True)
    creador= models.ForeignKey(
        User,
        related_name='procesos',
        null=True,
        on_delete=models.PROTECT

    )
    class Meta:
        app_label: 'app'




class Afiliacion(models.Model):
    id_individuo= models.ForeignKey(
        Individuo,
        related_name='afiliaciones',
        null=False,
        on_delete=models.PROTECT

    )
    id_partido= models.ForeignKey(
        Partido,
        related_name='afiliaciones',
        null=False,
        on_delete=models.PROTECT

    )
    fecha_ingreso=models.DateField(null=True)
    fecha_salida=models.DateField(null=True)
    aprobado=models.BooleanField(null=True)

    class Meta:
        app_label: 'app'


class Implicado(models.Model):
    id_afiliado= models.ForeignKey(
        Afiliacion,
        related_name='implicados',
        null=False,
        on_delete=models.PROTECT

    )
    id_proceso= models.ForeignKey(
        Proceso,
        related_name='implicados',
        null=False,
        on_delete=models.PROTECT

    ) 
    fecha= models.DateField(null=True)
    acusacion=models.CharField(max_length=45,null=True)
    culpable=models.BooleanField(null=True)
    pena=models.CharField(max_length=45,null=True)
    comentarios=models.CharField(max_length=45,null=True)
    
    class Meta:
        app_label: 'app'