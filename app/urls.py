from django.urls import path 
from . import views 

app_name = 'app' 
urlpatterns = [
    path('', views.index, name='index'), 
    path('', views.index, name='inicio'), 
    # login
    path('login/',views.iniciar_sesion_view, name='iniciar_sesion_view'),
    path('post_login',views.iniciar_sesion_post, name= 'iniciar_sesion_post'),
    # loout
    path('logout/',views.cerrar_sesion, name='cerrar_sesion'),
    # crear cuenta
    path('registro/',views.form_registro_view, name='form_registro_view'),
    path('registro_post/', views.registro_post, name='registro_post'),

# NUESTRO PROYECTOOOO

    path('cuenta',views.cuenta_view, name='cuenta_view'),
    path('cuenta_post/', views.cuenta_post, name='cuenta_post'),
    path('index',views.index_view, name='index_view'),
    path('index_post',views.index_post, name= 'index_post'),
    
    path('inicio',views.inicio_view, name='inicio_view'),
    path('inicioAdmin',views.inicioAdmin, name='inicioAdmin'),

    path('afiliacion',views.afiliacion_view, name='afiliacion_view'),
    path('aprobarAfiliacion',views.aprobarAfiliacion_view, name='aprobarAfiliacion_view'),
    path('aprobarIndividuo',views.aprobarIndividuo_view, name='aprobarIndividuo_view'),
    
    path('configure',views.configure_view, name='configure_view'),
    path('configure_post',views.configure_post, name='configure_post'),



    
  
    path('consultaLista',views.consultaLista_view, name='consultaLista_view'),
    path('consultaParti',views.consultaParti_view, name='consultaParti_view'),
    
    
    path('implicar',views.implicar_view, name='implicar_view'),
    

    # path('peliculas', views.peliculas_view, name='peliculas'),
    # path('peliculas/crear',views.form_crear_pelicula_view, name='form_crear_pelicula_view'),
    # path('peliculas/crear_post',views.crear_pelicula_post, name='crear_pelicula_post'),
    path('consultaLista',views.consultaLista_view, name='consultaLista_view'),
    path('consultaLista/<int:id>',views.consultALista_view, name='consultALista_view'),


    path('politico',views.politico_view, name='politico'),
    path('politico/crear',views.form_crear_politico_view, name='form_crear_politico_view'),
    path('politico/crear_post/', views.crear_politico_post, name='crear_politico_post'),
    
    path('habilitar',views.habilitar_view, name='habilitar_view'),
    path('habilitar/<int:id>',views.Habilitarr_view, name='Habilitarr_view'),
    path('habilitar_post',views.habilitar_post, name='habilitar_post'),

    path('aprobarProceso',views.aprobarProceso_view, name='aprobarProceso_view'),
    path('aprobarProceso/<int:id>',views.aprobarProcesoo_view, name='aprobarProcesoo_view'),
    path('aprobarProceso_post',views.aprobarProceso_post, name='aprobarProceso_post'),

    # path('consultaIndis',views.consultaIndis_view, name='consultaIndis_view'),
    path('consultaIndi',views.consultaIndi_view, name='consultaIndi_view'),
    path('consultaIndi/<int:id>',views.consultAIndi_view, name='consultAIndi_view'),
    path('aprobar_indivi_post',views.aprobar_indivi_post, name='aprobar_indivi_post'),
    
    path('individuo',views.individuo_view, name='individuo'),
    path('individuo/crear',views.form_crear_individuo_view, name='form_crear_individuo_view'),
    path('individuo/crear_post/',views.crear_individuo_post, name='crear_individuo_post'),

    path('consultaProce',views.consultaProce_view, name='consultaProce_view'),
    path('proceso',views.proceso_view, name='proceso_view'),
    path('proceso/crear',views.form_crear_proceso_view, name='form_crear_proceso_view'),
    path('proceso/crear_post/',views.crear_proceso_post, name='crear_proceso_post'),



    path('nosotros',views.nosotros_view, name='nosotros_view'),



]

 